Rails.application.routes.draw do
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    root "articles#index"

    get 'search',to: "articles#search"

    resources :articles do
      resources :comments
    end
    resources :tags, only: [:show]
  end
end
