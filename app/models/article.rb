class Article < ApplicationRecord
	include Visible
  
	has_many :comments, dependent: :destroy
	has_rich_text :body
	has_many :taggings, dependent: :destroy
	has_many :tags, through: :taggings, dependent: :destroy

	mount_uploader :image, ImageUploader
	validates :title, presence: true
	validates :body, presence: true, length: { minimum: 5 }

	def all_tags
		self.tags.map(&:name).join(', ')
	end
	def all_tags=(names)
		self.tags = names.split(',').map do | name |
			Tag.where(name: name.strip).first_or_create!	
		end 
	end

  end